# Step Project "Forkio"

#### Technologies:
- *Javascript Vanilla*
- *SCSS*
- *Gulp*

### Created by Oleg & Platon Kirii
*Worked together on*: Project structure, .gulp file, readme.md


#### Oleg
Sections:
- Revolutionary Editor
- Here is what you get
- Fork Subscription Pricing

#### Platon Kiriy

Sections:
- Header
- People Are Talking About Fork

### Prerequisite
- node.js >= 18.12.1
- npm >= 8.19.2

### Start local server

```sh
npm run dev
```

### Create production build

```sh
npm run build
```
